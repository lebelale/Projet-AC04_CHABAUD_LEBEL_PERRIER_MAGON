---
title: "Rapport de TP AC04"
author: "Baptiste CHABAUD, Alexis LEBEL, Lucien PERRIER, Henri MAGON"
date: "P23 - Rendu : 16/06/2023"
---

# Rapport de TP AC04

Ce rapport ainsi que les sources et le sujet sont dispnibles sur Gitlab, à l'adresse : https://gitlab.utc.fr/lebelale/Projet-AC04_CHABAUD_LEBEL_PERRIER_MAGON

## Membres du groupe de TP

|   Nom   |  Prénom  | Participation |
| :-----: | :------: | :-----------: |
| CHABAUD | Baptiste |      7.5%      |
|  LEBEL  |  Alexis  |      60%      |
| PERRIER |  Lucien  |      25%      |
|  MAGON  |  Henri   |      7.5%      |

## Données simulées

On fixe aléatoirement les valeurs de $a_0$, $b_0$ et $s_0$.

```{R}
a0=round(runif(1,3,15),2)
b0=round(runif(1,1,5),2)
s0=round(runif(1,1,3),2)
```

---

__Q.1A : Pour un n quelconque ($n \leq 200$), simuler n variables aléatoires indépendantes et identiquement distribuées $X1,…,Xn$ de variable aléatoire parente, X de la loi uniforme sur [0,5]. On note $(x_i)_{1\geq i\geq n}$ les réalisations des $(X_i)_i$. :__


```{R}
n=200 + sample(1:100, 1)
X=runif(n,0,5)
plot(X)
```

__Q.1b :  Simuler ensuite les variables aléatoires indépendantes Y1,…,Yn issues du modèle de régression linéaire Gaussien (voir section I.1) avec $a=a_0$, $b=b_0$ et $\sigma^2=s0$. On note $(y_i)_{1\geq i\geq n}$ les réalisations de $(Y_i)_{1\geq i\geq n}$; la suite des $(x_i,Y_i)_{1\geq i\geq n}$ satisfait le modèle (1) (voir section I.1) :__

```{R}
Y=rnorm(n,a0+b0*X,s0)
```

__Q.2 : Représenter sur un même graphique le nuage des points $(x_i,y_i)_{1\geq  i\geq  n}$ obtenus aux questions (Q1a) et (Q1b) ainsi que la droite de régression d’équation $y=a+bx$, avec $a=a_0$ et $b=b_0$ :__

```{R}
plot(X,Y)
abline(a=a0,b=b0,col="red")
# On change la couleur de la droite pour la différencier du nuage de points
```

__Q.3 : Créer les functions "b_estim”, "a_estim”, "sigma_estim”, qui prennent en argument les couples (xi,yi) 1\geq i\geq n de taille quelconque n et qui renvoient l’estimation par les estimateurs sans biais des paramètres b, a et $\sigma ^2$ :__ 

Pour que ce soit non-biaisé, on va utiliser la méthode des moindres carrés. On a donc :

$$a_0 = \frac{Covar(X,Y)}{Var(X)}$$

$$b_0 = \overline{Y} - a_0 \overline{X}$$

$$\sigma_0 = \sqrt{\frac{1}{n-1} (\sum_{i=1}^{n}y_i^2) - \overline{Y}^2}$$

On a le droit d'utiliser la covariance et la variance car on a supposé que les variables étaient indépendantes.

```{R}
b_estim=function(X,Y){
  return(cov(X,Y)/var(X))
}

a_estim=function(X,Y){
  return(mean(Y)-b_estim(X,Y)*mean(X))
}

sigma_estim=function(X,Y){
  return(sqrt((1/(length(X)-1))*(sum(Y^2)-mean(Y)^2)))
}
```

__Q.4 :  A partir du jeu de données simulées aux questions (Q1a) et (Q1b), en déduire l’estimation de a, b et $\sigma^2$ :__

Ainsi, les estimations sont :

```{R}
b_estim(X,Y)
a_estim(X,Y)
sigma_estim(X,Y)
```

__Q.5 : Représenter sur un même graphe, le nuage de points $(x_i,y_i)_{1\geq i\geq n}$ simulés en (Q1a) et (Q1b), la droite de régression y=a0+b0x ainsi que la droite des moindres carrés (utiliser différentes couleurs sur le graphique pour distinguer ces deux droites). :__

Graphe des points et de la droite de régression avec les paramètres estimés :

```{R}
plot(X,Y)
abline(a=a_estim(X,Y),b=b_estim(X,Y),col="red")
abline(a=a0,b=b0,col="blue")
```

__Q.6 : Déterminer le vecteur des résidus $\hat{\epsilon}_i$ et vérifier la propriété des résidus :__

Dans ce cas, il suffit de calculer la différence entre les valeurs de $Y$ et les valeurs de $\hat{Y}$.

```{R}
residus=Y-(a_estim(X,Y)+b_estim(X,Y)*X)
```

On essaye de voir si on a bien une répartition qui ressemble à une normale centrée sur 0 des résidus :

```{R}
plot(density(residus))
```

Cette répartition semble bien être une normale centrée sur 0.

__Q.7 : Vérifier que $(\overline{x},\overline{y})$ appartient à la droite des moindres carrés :__

```{R}
plot(mean(X),mean(Y),col="red")
abline(a=a0,b=b0,col="blue")
```

__Q.8 : Linear Model :__

Exemple de régression linéaire avec R :

```{R}	
donnees <- data.frame(varx = c(0, 0.2, 0.3, 0.6), vary = c(1.01, 1.44, 1.55, 2.1))
reg<-lm(vary~varx, data = donnees)
summary(reg)
```

__Q.9 : En utilisant les fonctions "$a_{estim}$" et "$b_{estim}$", illustrer graphiquement la convergence en probabilité de $\hat{a}$ et $\hat{b}$ vers a=a0 et b=b0 quand n la taille de l’échantillon grandit. :__

```{R}
a0
b0

n_values=c(5, 10, 20, 50, 100, 500, 1000, 5000, 10000)

# On initialise les vecteurs qui vont contenir les valeurs de a et b estimées
hat_a_values=rep(0,length(n_values))
hat_b_values=rep(0,length(n_values))

for (i in 1:length(n_values)){
  n=n_values[i]
  X=runif(n,0,5)
  Y=rnorm(n,a0+b0*X,s0)
  hat_a_values[i]=a_estim(X,Y)
  hat_b_values[i]=b_estim(X,Y)
}

plot(n_values,hat_a_values,col="red", log="x", type="b") # on prend une échelle logarithmique pour mieux voir la convergence
abline(a=a0,b=0,col="blue")
plot(n_values,hat_b_values,col="red", log="x", type="b") # on prend une échelle logarithmique pour mieux voir la convergence
abline(a=b0,b=0,col="blue")
```

__Q.10 : En utilisant les fonctions “replicate”, a_estim”, et “b_estim”, illustrer le fait que :__

$$ \frac{\hat{a}-a_0}{\sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})}} \sim \mathcal{T}(n-2) $$

```{R}
n=5000
count=150

# On crèe un vecteur, qui va contenir 150 realisations de n couples (X,Y)
for (i in 1:count){
  X=runif(n,0,5)
  Y=rnorm(n,a0+b0*X,s0)
  if (i==1){
    donnees=data.frame(X,Y)
  } else {
    donnees=rbind(donnees,data.frame(X,Y))
  }
}

# On crée un vecteur qui va contenir 150 realisations de a estimé
for (i in 1:count){
  if (i==1){
    a_estim_values=a_estim(donnees$X[1:n],donnees$Y[1:n])
  } else {
    a_estim_values=c(a_estim_values,a_estim(donnees$X[(i-1)*n+1:i*n],donnees$Y[(i-1)*n+1:i*n]))
  }
}

# Pour chacun de ces a estimés, on calcule la valeur de la statistique
stat_values=(a_estim_values-a0)/sqrt((sigma_estim(donnees$X[1:n],donnees$Y[1:n])^2/n)*(1+mean(donnees$X[1:n])^2/var(donnees$X[1:n])))

# On fait un histogramme de ces valeurs
hist(stat_values, freq=FALSE, breaks=50, col="lightblue", border="white", main="Histogramme de la statistique", xlab="Valeurs de la statistique", ylab="Densité")
# On superpose la densité de la loi de Student à n-2 degrés de liberté
curve(dt(x,n-2),add=TRUE,col="red",lwd=2)
```

On peut voir ici que la densité de la loi de Student à n-2 degrés de liberté semble bien correspondre à la distribution de la statistique, bien que parfois un peu décalée. Si on augmente N, on devrait obtenir une meilleure approximation (asympotiquement).


__Question 11 : Créer les fonctions “gen_IC_a”, “gen_IC_b” et “gen_IC_s” qui prennent en argument le vecteur des observations $(x_i, y_i)_{1\leq i \leq n}$ de taille quelconque n et un niveau de confiance $1−\alpha$ et renvoient un intervalle de confiance bilatéral pour a, b et $\sigma^2$ sous forme d’un vecteur de longueur 2 contenant la borne inférieure et la borne supérieure.__

En partant de la question précédente, qui nous dit que la statistique :

$$ \frac{\hat{a}-a_0}{\sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})}} \sim \mathcal{T}(n-2) $$

Donc suivait une loi de Student à n-2 degrés de liberté, on peut en déduire que cette fonction est une fonction pivotale. On va donc sortir a de cette statistique, et en déduire un intervalle de confiance bilatéral pour a, en utilisant les quantiles de la loi de Student à n-2 degrés de liberté.

Cela va nous donner :

$$\hat{a} - a_0 = \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)$$

$$ \Leftrightarrow \hat{a} = a_0 + \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)$$

La partie dont dépends le quantile de la loi de Student est donc une variable aléatoire, et on peut donc en déduire un intervalle de confiance bilatéral pour a :

$$ \mathbb{P}(\hat{a} - \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2) \leq a_0 \leq \hat{a} + \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)) = 1 - \alpha $$

On en déduit donc :

$$ IC = [\hat{a} - \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2) ; \hat{a} + \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)] $$

```{R}	
gen_IC_a=function(x,y,alpha){
  n=length(x)
  a_estim_value=a_estim(x,y)
  s2x = sum((x-mean(x))^2)
  sigma_estim_value=sigma_estim(x,y)
  borne_inf=a_estim_value-sqrt((sigma_estim_value/n)*(1+(mean(x)^2/s2x^2)))*qt(1-(alpha/2),n-2)
  borne_sup=a_estim_value+sqrt((sigma_estim_value/n)*(1+(mean(x)^2/s2x^2)))*qt(1-(alpha/2),n-2)
  return(c(borne_inf,borne_sup))
}

# On teste la fonction
gen_IC_a(donnees$X[1:n],donnees$Y[1:n],0.05)
# Fonction integrée dans R
summary(lm(donnees$Y[1:n]~donnees$X[1:n]))$coefficients
# en utilisant la fonction d'intervalle de confiance de R
confint(lm(donnees$Y[1:n]~donnees$X[1:n]),level=0.95)
# Essais à la main : ça semble correct...
```

On peut faire de même pour b, parce qu'on sait que la relation qui lie a et b est :

$$ \hat{b} = \bar{y} - \hat{a}\bar{x} $$

En retournant la formule, on arrive à :

$$ \hat{a} = \frac{\bar{y} - \hat{b}\bar{x}}{\bar{x}} $$

$$ \Leftrightarrow a_0 + \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2) = \frac{\bar{y} - \hat{b}\bar{x}}{\bar{x}} $$

$$ \Leftrightarrow \hat{b} = \frac{\bar{y} - a_0\bar{x} - \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)}{\bar{x}} $$

On récupère la partie qui dépend du quantile de la loi de Student :

$$\hat{b} - \frac{\sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)}{\bar{x}} = \frac{\bar{y} - a_0\bar{x}}{\bar{x}} $$

On en déduit donc :

$$ \mathbb{P}(\hat{b} - \frac{\sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)}{\bar{x}} \leq \frac{\bar{y} - a_0\bar{x}}{\bar{x}} \leq \hat{b} + \frac{\sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)}{\bar{x}}) = 1 - \alpha $$

On simplifie :

$$ \mathbb{P}(\hat{b} - \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2) \leq \bar{y} - a_0\bar{x} \leq \hat{b} + \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)) = 1 - \alpha$$

or, $\bar{y} - a_0\bar{x} = b_0$, donc :

$$ \mathbb{P}(\hat{b} - \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2) \leq b_0 \leq \hat{b} + \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)) = 1 - \alpha$$

Ce qui donne :

$$IC_b = [\hat{b} - \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2) ; \hat{b} + \sqrt{\frac{\hat{\sigma^2}}{n}(1+\frac{\bar{x^2}}{s_x^2})} \mathcal{T}(n-2)]$$

```{R}
gen_IC_b=function(x,y,alpha){
  n=length(x)
  a_estim_value=a_estim(x,y)
  b_estim_value=b_estim(x,y)
  s2x = sum((x-mean(x))^2)
  sigma_estim_value=sigma_estim(x,y)
  borne_inf=b_estim_value-sqrt((sigma_estim_value/n)*(1+mean(x)^2/s2x))*qt(1-alpha/2,n-2)
  borne_sup=b_estim_value+sqrt((sigma_estim_value/n)*(1+mean(x)^2/s2x))*qt(1-alpha/2,n-2)
  return(c(borne_inf,borne_sup))
}

# On teste la fonction
gen_IC_b(donnees$X[1:n],donnees$Y[1:n],0.05)
# Fonction integrée dans R
summary(lm(donnees$Y[1:n]~donnees$X[1:n]))$coefficients
# Essais à la main : ça semble correct... @alebel
```

Dernière étape, on clacule l'IC pour sigma. On sait que lors d'une régression linéaire, les résidus suivent une loi normale d'espérance 0 et de variance $\sigma^2$. 

l'estimateur sans biais de $\sigma^2$ est :

$$ \hat{\sigma^2} = \frac{1}{n-1} \sum_{i=1}^{n} (y_i - \bar{y})^2 $$

En utilisant le théorème de Fisher, on sait

$$ \frac{(n-2)\hat{\sigma^2}}{\sigma^2} \sim \chi^2(n-2) $$

On en déduit donc :

$$ \mathbb{P}(\chi^2_{1-\frac{\alpha}{2}}(n-2) \leq \frac{(n-2)\hat{\sigma^2}}{\sigma^2} \leq \chi^2_{\frac{\alpha}{2}}(n-2)) = 1 - \alpha $$

CE qui donne l'intervalle de confiance :

$$ IC_{\sigma^2} = [\frac{(n-2)\hat{\sigma^2}}{\chi^2_{\frac{\alpha}{2}}(n-2)} ; \frac{(n-2)\hat{\sigma^2}}{\chi^2_{1-\frac{\alpha}{2}}(n-2)}] $$

```{R}
gen_IC_sigma=function(x,y,alpha){
  n=length(x)
  sigma_estim_value=sigma_estim(x,y)
  borne_inf=(n-1)*sigma_estim_value/qchisq(1-alpha/2,n-2)
  borne_sup=(n-1)*sigma_estim_value/qchisq(alpha/2,n-2)
  return(c(borne_inf,borne_sup))
}

# On teste la fonction
gen_IC_sigma(donnees$X[1:n],donnees$Y[1:n],0.05)
# Fonction empirique (faute de trouver mieux integré dans R..)
sigma_estim(donnees$X[1:n],donnees$Y[1:n])
# Essais à la main : ça semble correct... @alebel
```

__Q.12 : En utilisant la fonction “replicate”, créer 100 intervalles de confiance pour $a$, $b$ et $\sigma^2$.__


```{R}
IC_a = matrix(0,2,100)

#IC_a[,1] = gen_IC_a(donnees$X[1:n],donnees$Y[1:n],0.05)
#IC_a[,2] = gen_IC_a(donnees$X[(n+1):(2*n)],donnees$Y[(n+1):(2*n)],0.05)

for (i in 1:100){
  IC_a[,i] = gen_IC_a(donnees$X[((i-1)*n+1):(i*n)],donnees$Y[((i-1)*n+1):(i*n)],0.05)
}
```

__Q.13 : Pour visualiser les intervalles de confiance, la fonction “plot_ICs” est disponible dans le fichier “utils.R” à télécharger sous moodle et à sourcer par la commande source(“Utils.R”). Visualiser les intervalles de confiance de la question (Q12) et commentez-les.__

On sait que :

```{R}
source("sujet/utils.R")
# plot_ICs <- function(ICs, mu, plot = TRUE, xlim,main)

plot_ICs(IC_a, a0)
a0
```

On peut voir qu'une proportion de ces intervalles (qui tend vers 5% asymptotiquement) prédisent un encadrement de la valeur de a faux. Cela est dû au fait que l'on a fixé $\alpha$ à 5% et que l'on a effectué 150 tests. On s'attend donc à avoir 5% de faux positifs.

__Question 14__ : Représentation des jeux de données Anscombe :

```{R}
plot(anscombe$x1,anscombe$y1)
plot(anscombe$x2,anscombe$y2)
plot(anscombe$x3,anscombe$y3)
plot(anscombe$x4,anscombe$y4)
```

__Question 15__ : Régression linéaire sur les jeux de données Anscombe :

```{R}
reg1=lm(y1~x1, data = anscombe)
summary(reg1)
reg2=lm(y2~x2, data = anscombe)
summary(reg2)
reg3=lm(y3~x3, data = anscombe)
summary(reg3)
reg4=lm(y4~x4, data = anscombe)
summary(reg4)

plot(anscombe$x1,anscombe$y1)
abline(a=reg1$coefficients[1],b=reg1$coefficients[2],col="red")
```

Ici, les points ne suivent pas la droite, mais la droite suit la tendance générale des points. La régression linéaire n'est peut-être pas la meilleure méthode pour modéliser ces données. Néanmoins, on ne peut pas conclure sur la qualité de la régression linéaire sans savoir ce que représentent ces données.

```{R}	
plot(anscombe$x2,anscombe$y2)
abline(a=reg2$coefficients[1],b=reg2$coefficients[2],col="red")
```  

Ici, la régression linéaire n'est pas adaptée, car les points suivent plutôt une parabole. On peut donc conclure que la régression linéaire n'est pas adaptée pour modéliser ces données, qui selon toute vraisemblance représentent une parabole.

```{R}
plot(anscombe$x3,anscombe$y3)
abline(a=reg3$coefficients[1],b=reg3$coefficients[2],col="red")
```

Ici, la régression linéaire __semble__ adaptée pour modéliser ces données, qui selon toute vraisemblance représentent une droite.

```{R}
plot(anscombe$x4,anscombe$y4)
abline(a=reg4$coefficients[1],b=reg4$coefficients[2],col="red")
```

Ici, rien ne semble indiquer que la régression linéaire soit adaptée pour modéliser ces données. On ne peut pas conclure sans savoir ce que représentent ces données. En tout cas, la droite de régression ne suit pas la tendance générale des points.

Le problème de la régression, est qu'elle peut donner lieu à de fausses conclusions sur la nature des données. Il est toujours possible de faire passer une courbe par tous les points, sans pour autant que cela soit pertinent (ex: courbe de degré n-1 pour n points). Il faut donc faire attention à la nature des données, et ne pas se contenter de la régression.

__Question 16__ : Diagnostique graphique de la validité des résultats de la régression linéaire :

Les résidus corrigés sont définis par :

$$\hat{\epsilon}_i = \frac{\hat{\epsilon}_i}{\sqrt{1-h_{i}}}$$

et

$$h_i = \frac{1}{n} + \frac{(x_i - \overline{x})^2}{\sum_{j=1}^{n}(x_j - \overline{x})^2}$$


Pour chaque jeux de données, on calcule les résidus corrigés :

```{R}
residus_corriges1=reg1$residuals/sqrt(1-(1/length(anscombe$x1))-((anscombe$x1-mean(anscombe$x1))^2/sum((anscombe$x1-mean(anscombe$x1))^2)))
residus_corriges2=reg2$residuals/sqrt(1-(1/length(anscombe$x2))-((anscombe$x2-mean(anscombe$x2))^2/sum((anscombe$x2-mean(anscombe$x2))^2)))
residus_corriges3=reg3$residuals/sqrt(1-(1/length(anscombe$x3))-((anscombe$x3-mean(anscombe$x3))^2/sum((anscombe$x3-mean(anscombe$x3))^2)))
residus_corriges4=reg4$residuals/sqrt(1-(1/length(anscombe$x4))-((anscombe$x4-mean(anscombe$x4))^2/sum((anscombe$x4-mean(anscombe$x4))^2)))
```

1. On représente ces valeurs corrigées, tout d'abord par un diagramme quantile-quantile :

```{R}
qqnorm(residus_corriges1, main="Diagramme quantile-quantile des résidus corrigés du jeu de données 1", ylim=c(-3,3))
qqline(residus_corriges1, col="red")

hist(residus_corriges1, main="Histogramme des résidus corrigés du jeu de données 1")
curve(dnorm(x, mean=mean(residus_corriges1), sd=sd(residus_corriges1)), col="red", add=TRUE, lwd=2)
```


```{R}	
qqnorm(residus_corriges2, main="Diagramme quantile-quantile des résidus corrigés du jeu de données 2", ylim=c(-3,3))
qqline(residus_corriges2, col="red")

hist(residus_corriges2, main="Histogramme des résidus corrigés du jeu de données 2")
curve(dnorm(x, mean=mean(residus_corriges2), sd=sd(residus_corriges2)), col="red", add=TRUE, lwd=2)
```

```{R}
qqnorm(residus_corriges3, main="Diagramme quantile-quantile des résidus corrigés du jeu de données 3", ylim=c(-3,3))
qqline(residus_corriges3, col="red")

hist(residus_corriges3, main="Histogramme des résidus corrigés du jeu de données 3")
curve(dnorm(x, mean=mean(residus_corriges3), sd=sd(residus_corriges3)), col="red", add=TRUE)
```

```{R}
qqnorm(residus_corriges4, main="Diagramme quantile-quantile des résidus corrigés du jeu de données 4", ylim=c(-3,3))
qqline(residus_corriges4, col="red")

hist(residus_corriges4, main="Histogramme des résidus corrigés du jeu de données 4")
curve(dnorm(x, mean=mean(residus_corriges4), sd=sd(residus_corriges4)), col="red", add=TRUE)
```

De ces quatres jeux de données, seul le troisième admet des résidus corrigés qui semblent suivre la droite. Les autres présentes des résidus importants, qui montrent que la régression linéaire n'est pas adaptée.

Le fait que les points suivent la droite, signifie que les résidus corrigés suivent une loi normale centrée sur 0.

Du côté de l'hypothèse de normalité des résidus, on peut voir que les résidus corrigés semblent suivre une loi normale. On peut donc conclure que l'hypothèse de normalité des résidus est vérifiée.

2. On représente ensuite les résidus standardisés en fonction des valeurs de la variable explicative, pour vérifier l'hypothèse d'homoscédasticité :

```{R}
plot(anscombe$x1,residus_corriges1, main="Résidus corrigés en fonction de la variable explicative du jeu de données 1")

plot(anscombe$x2,residus_corriges2, main="Résidus corrigés en fonction de la variable explicative du jeu de données 2")

plot(anscombe$x3,residus_corriges3, main="Résidus corrigés en fonction de la variable explicative du jeu de données 3")

plot(anscombe$x4,residus_corriges4, main="Résidus corrigés en fonction de la variable explicative du jeu de données 4")
```

## Partie 4 Données Réelles

__Question 17 : Choix du jeu de données__

```{R}	
rm(list=ls())
 
fat <- read.table("sujet/datasets/bodyfat.dat",skip=1)
 
head(fat)
summary(fat)
 
correl <- cor(fat$V1, fat$V3)
 
correl
```

Le coefficient de corrélation entre la variable V1 et V3 est de 0.8781, ce qui est assez élevé. On peut donc penser que la régression linéaire est adaptée.

__Question 18 : Régression linéaire__

```{R}
b_estim=function(X,Y){
  return(cov(X,Y)/var(X))
}
 
a_estim=function(X,Y){
  return(mean(Y)-b_estim(X,Y)*mean(X))
}
 
 
a <- a_estim(fat$V1, fat$V3)
b <- b_estim(fat$V1, fat$V3)
 
print(paste0("l'equation de la droite est : y =", b, "*X +", a))
 
 
 
mod <- lm(V1 ~ V3, data = fat)
 
plot(fat$V3, fat$V1)
abline(mod, col = "red")
```

__Question 19 : Intervalles de confiance__

Fonctions de la partie 3 :


```{R}
confint(mod, 'V3', level = 0.95) # Pente
```

__Question 20 : Coefficient R2__

```{R}
summary(mod)
```

Le coefficien $R^2$ est de 0.771, ce qui est assez élevé. On peut donc penser que la régression linéaire est adaptée.

__Question 21 : Corellation Empirique__

Vérifier que $R^2 = \frac{S^2_{xy}}{S^2_x S^2_y}$

```{R}
computed_r2 <- (cov(fat$V1, fat$V3)^2)/(var(fat$V1)*var(fat$V3))
computed_r2
```

__Question 22 : Test de Student__

On a :

$H_0 : b = 0$
$H_1 : b \neq 0$

La fonction pivotale pour b est :

$\frac{\hat{b}-b}{\sqrt{\frac{\hat{\sigma}^2}{nS^2_x}}}$

la forme de la région critique est une bande centrée sur 0, de largeur $[-k,k]$, avec $k$ tel que $P(|Z|>k)=\alpha$. On rejette donc l'hypothèse nulle si la valeur absolue de la statistique de test est supérieure à $k$.

en utilisant la fonction pivotale, on obtient :

$\frac{\hat{b}-b}{\sqrt{\frac{\hat{\sigma}^2}{nS^2_x}}} \sim \mathcal{T}_{n-2}$

$\Rightarrow \frac{b}{\sqrt{\frac{\hat{\sigma}^2}{nS^2_x}}} \sim -\mathcal{T}_{n-2} + \frac{\hat{b}}{\sqrt{\frac{\hat{\sigma}^2}{nS^2_x}}}$

$\Rightarrow P(|b| > - \mathcal{T}_{n-2} * \sqrt{\frac{\hat{\sigma}^2}{nS^2_x}} + \hat{b}) = \frac{\alpha}{2}$

```{R}

```

__Question 23 : Diagrammes Quantile-Quantile__

```{R}
res_corFat <- mod$residuals/sqrt(1-(1/length(fat$V1))-((fat$V1 - mean(fat$V1))^2/sum((fat$V1-mean(fat$V1))^2)))
res_corThigh <-mod$residuals/sqrt(1-(1/length(fat$V3))-((fat$V3 - mean(fat$V3))^2/sum((fat$V3-mean(fat$V3))^2)))
 
qqnorm(res_corFat)
qqline(res_corFat)
qqnorm(res_corThigh)
qqline(res_corThigh)
```

__Question 24 : Evaluer visuellement l’indépendance des résidus corrigés et l’hypothèse d’homoscédasticité à l’aide du graphe des résidus corrigés versus la variable explicative.__

```{R}
plot(fat$V3, res_corFat)
plot(fat$V1, res_corThigh)
```

Visuellement, il est impossible de prédire la position d'un point sur le plan, en fonction des points précédents. On peut donc penser que les résidus corrigés sont indépendants.

__Question 25 : Nouvelles valeurs de x__

On a :

$$\frac{\bar{Y}-E(Y_0)}{\sqrt{Var(Y_0)}} \rightarrow \mathcal{N}(0,1)$$

$$\Rightarrow I_C = [\bar{Y}-u_{1-\frac{\alpha}{2}}\sqrt{Var(Y_0)};\bar{Y}+u_{1-\frac{\alpha}{2}}\sqrt{Var(Y_0)}]$$

## Conclusion

Durant ce projet, nous avons vu différentes façons de calculer une régression linéaire, et de vérifier si elle est adaptée ou non. Nous avons également pu calculer des intervalles de confiance sur les paramètres de la régression, et vérifier si les hypothèses de la régression linéaire étaient vérifiées ou non (a travers des diagrammes quantile-quantile par exemple).

Ce projet montre que la régression Linéaire peut parfois être inadaptée, et qu'il faut donc vérifier les hypothèses et la cohérence des données avant de l'utiliser.